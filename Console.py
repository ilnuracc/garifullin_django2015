# coding=utf-8
import os
import shutil

print
WORKING = True


def manage(inp_list):
    if inp_list[0] in ("ls", "dir"):
        return dir(inp_list)
    elif inp_list[0] in ("cd"):
        if len(inp_list) == 1:
            print "Empty command"
        return cd(inp_list)
    elif inp_list[0] in ("cat"):
        if len(inp_list) == 1:
            print "Empty command"
        if len(inp_list) == 2:
            return cat_reading(inp_list)
        if inp_list[1] == ">":
            return cat_writing(inp_list)
    elif inp_list[0] == "rm":
        rm(inp_list)
    elif inp_list[0] == "supercopy":
        supercopy(inp_list)


def dir(inp_list):
    dirs = os.listdir(os.path.abspath(os.curdir))
    for item in dirs:
        print item


def cd(inp_list):
    if inp_list[1] == "..":
        os.chdir(os.pardir)
    elif inp_list[1] in os.listdir(os.curdir):
        os.chdir(os.path.join(os.curdir, inp_list[0]))


def cat_reading(inp_list):
    if inp_list[1] in os.listdir(os.curdir):
        file = open(inp_list[1], "r")
        for line in file:
            print line
        file.close()
    else:
        print "File not found"


def cat_writing(inp_list):
    if inp_list[2] in os.listdir(os.curdir):
        os.remove(inp_list[2])
    file = open(inp_list[2], "w+")
    string = raw_input("For finish, type \"|>\" \n<| ")
    while string[len(string) - 2:] != "|>":
        string += raw_input()
    file.write(string[:-2])
    file.close()


def rm(inp_list):
    try:
        os.remove(inp_list[1])
    except WindowsError:
        print "File already doesn't exist"


def supercopy(inp_list):
    if inp_list[1] in os.listdir(os.curdir):
        k = 0
        while k != int(inp_list[2]):
            shutil.copy(os.path.join(os.curdir, inp_list[1]),
                        os.path.join(os.curdir, "(copy %s) %s" % (k, inp_list[1])))
            k += 1


while WORKING:
    inp = raw_input("--- " + os.path.abspath(os.curdir) + " <> ")
    if not inp:
        continue

    inp_list = inp.split()
    if inp_list[0] == "exit":
        WORKING = False
    manage(inp_list)
    print
