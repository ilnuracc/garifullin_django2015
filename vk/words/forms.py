from django import forms
from words.models import Message


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['text', 'to']
