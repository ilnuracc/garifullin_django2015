from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render


# Create your views here.
from words.forms import MessageForm
from words.models import Message


@login_required(login_url=reverse_lazy("core:sign_in"))
def conversations(request):
    my_messages = Message.objects.filter(Q(sender=request.user) | Q(to=request.user))

    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            Message(sender=request.user, to=form.cleaned_data['to'], text=form.cleaned_data['text']).save()
            return HttpResponseRedirect(reverse_lazy("words:conversations"))
        else:
            return render(request, "words/conversations.html", {'form': form,
                                                                'messages': my_messages,
                                                                'title': 'Conversations'})

    return render(request, "words/conversations.html", {'form': MessageForm(),
                                                        'messages': my_messages,
                                                        'title': 'Conversations'})
