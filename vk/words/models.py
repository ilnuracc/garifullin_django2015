from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Message(models.Model):
    sender = models.ForeignKey(User, related_name='sender')
    to = models.ForeignKey(User, related_name='to')
    text = models.TextField()
    time = models.DateTimeField(auto_now=True)
