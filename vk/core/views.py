from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render


# Create your views here.
from core.forms import SignInForm


def sign_in(request):
    if request.method == 'POST':
        form = SignInForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password']
            )
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse("words:conversations"))
            else:
                return render(request, 'core/welcome.html', {'form': form})
        else:
            return render(request, 'core/welcome.html', {'form': form})
    else:
        return render(request, "core/welcome.html", {'form': SignInForm()})
