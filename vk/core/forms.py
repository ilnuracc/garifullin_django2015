from django import forms


class SignInForm(forms.Form):
    username = forms.CharField(label=False, widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    password = forms.CharField(label=False, widget=forms.TextInput(attrs={'placeholder': 'Password',
                                                                          'type': 'password'}))