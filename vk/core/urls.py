from django.conf.urls import include, url

urlpatterns = [
    url(r'^login/$', 'core.views.sign_in', name='sign_in'),
]
