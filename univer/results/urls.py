from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^query$', 'results.views.show', name='show'),
]
