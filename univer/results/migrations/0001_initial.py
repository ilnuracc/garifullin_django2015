# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StudentExam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student', models.CharField(max_length=20)),
                ('subject', models.CharField(max_length=20)),
                ('semester', models.SmallIntegerField(default=1)),
                ('planned', models.SmallIntegerField(default=100)),
                ('score', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'results',
                'verbose_name_plural': 'resultados',
            },
        ),
        migrations.AlterUniqueTogether(
            name='studentexam',
            unique_together=set([('student', 'subject', 'semester')]),
        ),
    ]
