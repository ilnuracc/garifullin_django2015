import json
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.utils.safestring import SafeString

from core.forms import SignInForm, SignUpForm
# from django_sse.redisqueue import RedisQueueView



# Create your views here.
from games.models import Calling, Game
from users.models import Profile


@login_required(login_url=reverse_lazy("home:welcome_page"))
def home_page(request):
    i = 0
    response = {}

    now_played = Game.objects.exclude(winner__isnull=False)
    played = Game.objects.exclude(winner__isnull=True)

    for game in now_played.iterator():
        response[i] = {"game_size": game.game_info.board_size,
                       "game_id": game.game_info.id,
                       "now": True,
                       "black": game.black.user.username,
                       "black_rating": game.black.rating,
                       "white": game.white.user.username,
                       "white_rating": game.white.rating}
        i += 1

    for game in played.iterator():
        response[i] = {"game_size": game.game_info.board_size,
                       "game_id": game.game_info.id,
                       "now": False,
                       "black": game.black.user.username,
                       "black_rating": game.black.rating,
                       "white": game.white.user.username,
                       "white_rating": game.white.rating}
        i += 1

    return render(request, "core/home.html", {"games": True,
                                               "callings": Calling.objects.exclude(accepter__isnull=False)[:10],
                                               "json": SafeString(json.dumps(response))})


def welcome_page(request):
    if request.method == 'POST':
        form = SignInForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password']
            )
            if user is not None:
                login(request, user)
                if "next" in request.GET:
                    redirect = HttpResponseRedirect(request.GET["next"])
                else:
                    redirect = HttpResponseRedirect(reverse_lazy("home:home_page"))
                redirect.set_cookie("who", request.user.username, max_age=24 * 60 * 60)
                return redirect
            else:
                return render(request, 'core/welcome.html', {"headline": "Welcome",
                                                             "form": form,
                                                             'wrong': 'Whoops, can\'t find you ><'})
        else:
            return render(request, 'core/welcome.html', {"headline": "Welcome",
                                                         "form": form})

    form = SignInForm()
    if "who" in request.COOKIES:
        form.fields["username"].initial = request.COOKIES["who"]
    return render(request, "core/welcome.html", {"headline": "Welcome",
                                                 "form": form})


def sign_out(requset):
    if requset.user.is_authenticated():
        logout(requset)
        return HttpResponseRedirect(reverse("home:welcome_page"))
    else:
        return render(requset, 'blank.html', {'headline': 'Maaan, you don\'t even signed in -_-"',
                                              'message': 'Lorem bla ipsum bla dolor bla sit bla amet bla bla.'})


def sign_up(request):
    if request.user.is_anonymous():
        if request.method == 'POST':
            form = SignUpForm(request.POST)
            if form.is_valid():
                user = authenticate(
                    username=form.cleaned_data['username'],
                    password=form.cleaned_data['password']
                )
                if user is None:
                    user = User.objects.create_user(form.cleaned_data['username'],
                                                    form.cleaned_data['email'],
                                                    form.cleaned_data['password'])
                    user.first_name = form.cleaned_data['first_name']
                    profile = Profile(user=user)
                    if form.cleaned_data['gender']:
                        profile.gender = form.cleaned_data['gender']
                    user.save()
                    profile.save()
                    new_one = authenticate(username=form.cleaned_data['username'],
                                           password=form.cleaned_data['password'])
                    login(request, new_one)
                    return HttpResponseRedirect(reverse_lazy("home:home_page"))
                else:
                    return render(request, "core/signup.html", {"headline": "Sign Up",
                                                                "form": form})
            else:
                return render(request, "core/signup.html", {"headline": "Sign Up",
                                                            "form": form})
        form = SignUpForm()
        return render(request, "core/signup.html", {"headline": "Sign Up",
                                                    "form": form})
    else:
        return render(request, 'blank.html', {'headline': 'Wow, do you wanna sign up twice?!',
                                              'message': 'I think, that i am not so ... hmm.. demanding programmer '
                                                         'to let you do that if you already signed in. Think by yourself, dude.'})
