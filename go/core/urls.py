from django.conf.urls import include, url
from django.views.generic import TemplateView
from core import views

urlpatterns = [
    url(r'^$', 'core.views.home_page', name='home_page'),
    url(r'^about/$', TemplateView.as_view(template_name="core/about.html"), name="about_page"),
    url(r'^welcome/$', 'core.views.welcome_page', name='welcome_page'),
    url(r'^signout/$', 'core.views.sign_out', name='sign_out'),
    url(r'^signup/$', 'core.views.sign_up', name='sign_up'),
]
