from django import forms

GENDER = (
    ('M', 'Male'),
    ('F', 'Female'),
)


class SignInForm(forms.Form):
    username = forms.CharField(label=False, widget=forms.TextInput(attrs={'placeholder': 'Username/Email'}))
    password = forms.CharField(label=False, widget=forms.TextInput(attrs={'placeholder': 'Password',
                                                                          'type': 'password'}))


class SignUpForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField()
    first_name = forms.CharField(required=False)
    gender = forms.ChoiceField(required=False, choices=GENDER)
