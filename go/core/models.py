from django.core.files import File
from django.db import models


# Create your models here
from users.models import Profile


class Collection(models.Model):
    file = models.FileField(upload_to='games')
    comment = models.TextField(blank=True)
    upload_time = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(Profile)

    def __unicode__(self):
        return self.file.name
