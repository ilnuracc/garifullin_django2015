from django.contrib import admin

# Register your models here.
from ladders.models import Ladder, LadderGame, LadderMember

admin.site.register(Ladder)
admin.site.register(LadderMember)
admin.site.register(LadderGame)