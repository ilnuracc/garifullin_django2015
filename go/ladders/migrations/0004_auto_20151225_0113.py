# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0012_auto_20151225_0113'),
        ('users', '0002_auto_20151220_2027'),
        ('ladders', '0003_auto_20151225_0109'),
    ]

    operations = [
        migrations.CreateModel(
            name='LadderGame',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caller', models.ForeignKey(related_name='ladder_caller', to='users.Profile')),
                ('game', models.ForeignKey(default=None, to='games.Game', null=True)),
                ('ladder', models.ForeignKey(to='ladders.Ladder')),
                ('opponent', models.ForeignKey(related_name='opponent', to='users.Profile')),
            ],
        ),
        migrations.RemoveField(
            model_name='laddercalling',
            name='caller',
        ),
        migrations.RemoveField(
            model_name='laddercalling',
            name='game',
        ),
        migrations.RemoveField(
            model_name='laddercalling',
            name='ladder',
        ),
        migrations.RemoveField(
            model_name='laddercalling',
            name='opponent',
        ),
        migrations.DeleteModel(
            name='LadderCalling',
        ),
    ]
