# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0011_auto_20151225_0109'),
        ('ladders', '0002_laddermember_defended'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='laddercalling',
            name='call',
        ),
        migrations.AddField(
            model_name='laddercalling',
            name='game',
            field=models.ForeignKey(default=None, to='games.Game', null=True),
        ),
    ]
