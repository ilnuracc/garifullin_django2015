# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('games', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ladder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=36)),
            ],
        ),
        migrations.CreateModel(
            name='LadderCalling',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('call', models.ForeignKey(to='games.Calling')),
                ('caller', models.ForeignKey(related_name='ladder_caller', to='users.Profile')),
                ('ladder', models.ForeignKey(to='ladders.Ladder')),
                ('opponent', models.ForeignKey(related_name='opponent', to='users.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='LadderMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('place', models.IntegerField(unique=True)),
                ('ladder', models.ForeignKey(to='ladders.Ladder')),
                ('member', models.ForeignKey(to='users.Profile')),
            ],
        ),
    ]
