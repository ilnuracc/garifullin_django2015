import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render


# Create your views here.
from games.models import GameInfo, Calling, Game
from ladders.models import LadderMember, Ladder, LadderGame
from users.models import Profile


@login_required(login_url=reverse_lazy("home:welcome_page"))
def show_page(request, ladder_id=None):
    if ladder_id is None:
        ladder_id = 1
    ladder = Ladder.objects.get(id=ladder_id)
    profile = Profile.objects.get(user=request.user)

    if request.method == 'GET':
        members = LadderMember.objects.filter(ladder=ladder)
        in_ladder = False
        try:
            member = members.get(member=profile)
            opponent = get_opponent(member, ladder_id)

        except ObjectDoesNotExist:
            member = None
            opponent = LadderMember.objects.order_by("place").last()

        if member:
            in_ladder = True

        return render(request, "ladders/ladder.html", {"headline": "Ladder " + ladder.title,
                                                       "ladder": ladder,
                                                       "members": members,
                                                       "opponent": opponent,
                                                       "in_ladder": in_ladder})
    elif request.method == 'POST':
        opponent = Profile.objects.get(user=User.objects.get(id=request.POST.get('opponent-id')))
        caller = Profile.objects.get(user=request.user)

        game_info = GameInfo(title='Ladder match',
                             handicap=0,
                             komi=6.5,
                             board_size=19,
                             main_basic_time=datetime.datetime(year=2015, month=1, day=3, hour=0, minute=0, second=0),
                             main_byo_yomi=datetime.datetime(year=2015, month=1, day=1, hour=0, minute=0, second=0),
                             main_byo_yomi_periods=5)
        game_info.save()

        call = Calling(game_info=game_info,
                       caller=Profile.objects.get(user=request.user),
                       accepter=Profile.objects.get(user=User.objects.get(id=request.POST.get('opponent-id'))))
        call.save()

        new_game = Game(game_info=game_info,
                        white=call.caller,
                        black=call.accepter)
        new_game.save()

        return HttpResponseRedirect(reverse_lazy("games:show_page", args=[game_info.id]))


@login_required
def join(request, ladder_id=None):
    if request.method == 'POST':
        member = LadderMember(ladder=Ladder.objects.get(id=ladder_id),
                              member=Profile.objects.get(user=request.user),
                              place=LadderMember.objects.count() + 1)
        member.save()
        return HttpResponseRedirect(reverse_lazy("ladders:show_page"))


def get_opponent(m, ladder_id):
    ladder = Ladder.objects.get(id=ladder_id)

    if m:
        if m.place == 1:
            return None
        else:
            return LadderMember.objects.get(place=m.place - 1, ladder=ladder)
    else:
        return None


def update_ladders(game):
    ladder_game = LadderGame.objects.get(game=game)
    ladder = ladder_game.ladder

    if game.winner == ladder_game.caller:
        winner = LadderMember.objects.get(member=ladder_game.caller, ladder=ladder)
        loser = LadderMember.objects.get(member=ladder_game.opponent, ladder=ladder)
        new_place = loser.place
        winner.place = 0
        loser.place = winner.place
        winner.place = new_place
    else:
        winner = LadderMember.objects.get(member=ladder_game.caller, ladder=ladder)
        winner.defended += 1
