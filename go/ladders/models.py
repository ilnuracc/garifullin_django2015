from django.db import models


# Create your models here.
from games.models import Calling, Game
from users.models import Profile


class Ladder(models.Model):
    title = models.CharField(max_length=36)

    def __unicode__(self):
        return self.title


class LadderMember(models.Model):
    ladder = models.ForeignKey(Ladder)
    member = models.ForeignKey(Profile)
    place = models.IntegerField(unique=True)
    defended = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return "%s | %s" % (self.place, self.member.user.username)


class LadderGame(models.Model):
    ladder = models.ForeignKey(Ladder)
    game = models.ForeignKey(Game, null=True, default=None)
    caller = models.ForeignKey(Profile, related_name='ladder_caller')
    opponent = models.ForeignKey(Profile, related_name='opponent')