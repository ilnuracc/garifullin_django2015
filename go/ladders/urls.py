from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^(?P<ladder_id>\d+)?$', 'ladders.views.show_page', name='show_page'),
    url(r'^(?P<ladder_id>\d+)/join$', 'ladders.views.join', name='join'),
]
