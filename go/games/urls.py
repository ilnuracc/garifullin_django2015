from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from games.views import CreatePageView

urlpatterns = [
    url(r'^new$', login_required(CreatePageView.as_view(), login_url=reverse_lazy('core:welcome')), name='new'),
    url(r'^games/(?P<game_id>\d+)?$', 'games.views.show_page', name='show_page'),
    url(r'^games/(?P<game_id>\d+)/move$', 'games.sgf.record'),
    url(r'^games/(?P<game_id>\d+)/load$', 'games.sgf.load'),
    url(r'^games/(?P<game_id>\d+)/time$', 'games.sgf.time'),
    url(r'^games/(?P<game_id>\d+)/status$', 'games.game.status'),
    url(r'^games/(?P<game_id>\d+)/surrender$', 'games.game.surrender'),
    url(r'^games/(?P<game_id>\d+)/comments$', 'games.game.comments', name='comments'),
    url(r'^callings/(?P<call_id>\d+)?$', 'games.views.show_callings', name='show_callings'),
    url(r'^callings/(?P<call_id>\d+)/accept$', 'games.views.accept', name='accept'),
    url(r'^callings/check', 'games.views.calls_check'),
    url(r'^callings/recd', 'games.views.received_accept'),
]
