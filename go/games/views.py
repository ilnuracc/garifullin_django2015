import datetime
import json
from django.utils.safestring import SafeString
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

# Create your views here
from games.forms import NewGameForm
from games.models import Calling, GameInfo, Game
from games.sgf import _initiate
from users.models import Profile


@login_required(login_url=reverse_lazy("home:welcome_page"))
def show_page(request, game_id=None):
    if game_id is not None:
        game_info = GameInfo.objects.get(id=game_id)
        game = Game.objects.get(game_info=game_info)
        return render(request, "games/game.html", {"headline": game_info.title,
                                                   "game_info": game_info,
                                                   "game": game,
                                                   "user": request.user,
                                                   "comments": GameInfo.objects.get(id=game_id).comments.all().order_by('-pub_date').iterator(),
                                                   "comments_l": GameInfo.objects.get(id=game_id).comments.count()})

    else:
        i = 0
        response = {}

        now_played = Game.objects.exclude(winner__isnull=False)
        played = Game.objects.exclude(winner__isnull=True)

        for game in now_played.iterator():
            response[i] = {"game_size": game.game_info.board_size,
                           "game_id": game.game_info.id,
                           "now": True,
                           "black": game.black.user.username,
                           "black_rating": game.black.rating,
                           "white": game.white.user.username,
                           "white_rating": game.white.rating}
            i += 1

        for game in played.iterator():
            response[i] = {"game_size": game.game_info.board_size,
                           "game_id": game.game_info.id,
                           "now": False,
                           "black": game.black.user.username,
                           "black_rating": game.black.rating,
                           "white": game.white.user.username,
                           "white_rating": game.white.rating}
            i += 1

        return render(request, "games/grid.html", {"headline": "All games",
                                                   "json": SafeString(json.dumps(response))})


@login_required(login_url=reverse_lazy("home:welcome_page"))
def show_callings(request, call_id=None):
    if call_id is not None:
        return render(request, "games/calls.html", {'callings': True})
    return render(request, "games/calls.html", {'headline': "Callings",
                                                'callings': Calling.objects.exclude(accepter__isnull=False),
                                                'hours': datetime.time(hour=2),
                                                'minutes': datetime.time(minute=1),
                                                'seconds': datetime.time(second=1)})


class CreatePageView(View):
    form_class = NewGameForm
    template_name = 'games/new.html'
    headline = 'New game'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'headline': self.headline, 'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            game_info = GameInfo(title=form.cleaned_data['title'],
                                 handicap=form.cleaned_data['handicap'],
                                 komi=form.cleaned_data['komi'],
                                 board_size=form.cleaned_data['board_size'],
                                 main_basic_time=form.cleaned_data['main_basic_time'],
                                 main_byo_yomi=form.cleaned_data['main_byo_yomi'],
                                 main_byo_yomi_periods=form.cleaned_data['main_byo_yomi_periods'])
            game_info.save()

            call = Calling(game_info=game_info,
                           caller=Profile.objects.get(user=request.user))
            call.save()

            return HttpResponseRedirect(reverse_lazy("games:show_callings"))

        else:
            render(request, self.template_name, {'headline': self.headline, 'form': form})


@login_required(login_url=reverse_lazy("home:welcome_page"))
def accept(request, call_id=None):
    if call_id and request.method == 'POST':

        call = Calling.objects.get(id=call_id)
        user = Profile.objects.get(user=request.user)

        if call.caller.rating >= user.rating:
            new_game = Game(game_info=call.game_info,
                            white=call.caller,
                            black=user)
        else:
            new_game = Game(game_info=call.game_info,
                            white=user,
                            black=call.caller)

        new_game.save()
        _initiate(call.game_info.id)

        call.accepter = user
        call.accepted = True
        call.save()
        return HttpResponseRedirect(reverse_lazy("games:show_page", args=[call.game_info.id]))
    return HttpResponseRedirect(reverse_lazy("games:show_callings"))


@login_required
def calls_check(request):
    user = Profile.objects.get(user=request.user)
    calls = Calling.objects.filter(caller=user).exclude(accepted=True).exclude(accepter__isnull=True)

    i = 0
    response = {}
    for item in calls:
        response[i] = reverse('games:show_page', args=[Calling.objects.get(id=item.id).game_info.id])
        i += 1

    return HttpResponse(json.dumps(response))


@login_required
def received_accept(request):
    if request.method == 'POST':
        id = request.POST['game_id']
        call = Calling.objects.get(game_info=GameInfo.objects.get(id=id))
        print 1
        call.accepted = True
        call.save()
        return HttpResponse("Ok")


