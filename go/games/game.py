import json
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import math
from games.models import GameInfo
from games.models import Game
from ladders.views import update_ladders
from users.models import Profile
from words.models import Comment


@login_required()
def surrender(request, game_id=None):
    if request.method == 'POST':
        player = request.POST['surrender']
        game_info = GameInfo.objects.get(id=game_id)
        game = Game.objects.get(game_info=game_info)

        if player == 'B':
            game_info.result = 'W'
            game.winner = game.white
            loser = game.black
        else:
            game_info.result = 'B'
            game.winner = game.black
            loser = game.white

        game.winner.rating = calculate_rating(game.black.rating.__int__(),
                                              game.white.rating.__int__(),
                                              game.winner.rating.__int__(),
                                              1)
        new_loser_rate = calculate_rating(game.black.rating.__int__(),
                                          game.white.rating.__int__(),
                                          game.winner.rating.__int__(),
                                          0)
        if 0 > loser.rating - new_loser_rate:
            print 1
            print int(loser.rating - new_loser_rate)
            loser.rating = 0
        else:
            print 2
            print int(new_loser_rate)
            loser.rating -= new_loser_rate

        game.winner.save()
        loser.save()
        game.save()
        game_info.save()

        update_ladders(game)
        return HttpResponse("Ok")


@login_required
def status(request, game_id):
    response = {0: True}
    game_info = GameInfo.objects.get(id=game_id)
    if game_info.result:
        response[0] = False
    response[1] = game_info.result
    return HttpResponse(json.dumps(response))


def calculate_rating(black, white, winner, S):
    D = black - white
    x = D / (black + 1)
    expected_score_black = 1 / (math.exp(x) + 1)
    expected_score_white = 1 - expected_score_black
    if winner == black:
        result = winner + 10 * (S - expected_score_black)
    else:
        result = winner + 10 * (S - expected_score_white)
    return result


@login_required
def comments(request, game_id):
    response = {}
    game_info = GameInfo.objects.get(id=game_id)

    if request.method == 'POST':
        c = Comment(sender=Profile.objects.get(user=request.user),
                    text=request.POST['text'])
        c.save()
        game_info.comments.add(c)

        time = '(%s:%s, %s.%s.%s)' % (c.pub_date.hour, c.pub_date.minute, c.pub_date.day, c.pub_date.month, c.pub_date.year)
        response[0] = {'me': True, 'sender': c.sender.user.username, 'text': c.text, 'pub_date': time, 'rating': c.sender.rating}
        return HttpResponse(json.dumps(response))

    else:
        i = 0
        for item in game_info.comments.iterator():
            time = '(%s:%s, %s.%s.%s)' % (item.pub_date.hour, item.pub_date.minute, item.pub_date.day, item.pub_date.month, item.pub_date.year)
            c = {'me': False, 'sender': item.sender.user.username, 'text': item.text, 'pub_date': time, 'rating': item.sender.rating}
            if item.sender.user == request.user:
                c['me'] = True
            response[i] = c
            i += 1

        return HttpResponse(json.dumps(response))