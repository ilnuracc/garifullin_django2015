from django import forms
from games.models import Calling, GameInfo

BOARD_SIZE = (
    ('19', '19x19'),
    ('13', '13x13'),
    ('9', '9x9')
)

BASIC_TIME = (
    ('00:00:00', 'None'),
    ('00:00:30', '30 seconds'),
    ('00:00:45', '45 seconds'),
    ('00:01:00', '1 minute'),
    ('00:01:15', '1 minute 15 seconds'),
    ('00:01:30', '1 minute 30 seconds'),
    ('00:01:45', '1 minute 45 seconds'),
    ('00:02:00', '2 minutes'),
    ('00:02:30', '2 minutes 30 seconds'),
    ('00:03:00', '3 minutes'),
    ('00:03:30', '3 minutes 30 seconds'),
    ('00:04:00', '4 minutes'),
    ('00:04:30', '4 minutes 30 seconds'),
    ('00:05:30', '5 minutes'),
    ('00:06:00', '6 minutes'),
    ('00:07:00', '7 minutes'),
    ('00:08:00', '8 minutes'),
    ('00:09:00', '9 minutes'),
    ('00:10:00', '10 minutes'),
    ('00:15:00', '15 minutes'),
    ('00:20:00', '20 minutes'),
    ('00:25:00', '25 minutes'),
    ('00:30:00', '30 minutes'),
    ('00:35:00', '35 minutes'),
    ('00:40:00', '40 minutes'),
    ('00:45:00', '45 minutes'),
    ('00:50:00', '50 minutes'),
    ('00:55:00', '55 minutes'),
    ('01:00:00', '1 hour'),
    ('01:15:00', '1 hour 15 minutes'),
    ('01:30:00', '1 hour 30 minutes'),
    ('01:45:00', '1 hour 45 minutes'),
    ('02:00:00', '2 hours'),
    ('02:15:00', '2 hours 15 minutes'),
    ('02:30:00', '2 hours 30 minutes'),
    ('02:45:00', '2 hours 45 minutes'),
    ('03:00:00', '3 hours')
)

BYO_YOMI = (
    ('00:00:10', '10 seconds'),
    ('00:00:12', '12 seconds'),
    ('00:00:15', '15 seconds'),
    ('00:00:20', '20 seconds'),
    ('00:00:25', '25 seconds'),
    ('00:00:30', '30 seconds'),
    ('00:00:35', '35 seconds'),
    ('00:00:40', '40 seconds'),
    ('00:00:45', '45 seconds'),
    ('00:00:50', '50 seconds'),
    ('00:00:55', '55 seconds'),
    ('00:01:00', '1 minute'),
    ('00:01:10', '1 minutes 10 seconds'),
    ('00:01:20', '1 minutes 20 seconds'),
    ('00:01:30', '1 minutes 30 seconds'),
    ('00:01:40', '1 minutes 40 seconds'),
    ('00:01:50', '1 minutes 50 seconds'),
    ('00:02:00', '2 minutes'),
    ('00:02:20', '2 minutes 20 seconds'),
    ('00:02:40', '2 minutes 40 seconds'),
    ('00:03:00', '3 minutes'),
    ('00:03:30', '3 minutes 30 seconds'),
    ('00:04:00', '4 minutes'),
    ('00:04:30', '4 minutes 30 seconds'),
    ('00:05:30', '5 minutes'),
    ('00:06:00', '6 minutes'),
    ('00:07:00', '7 minutes'),
    ('00:08:00', '8 minutes'),
    ('00:09:00', '9 minutes'),
    ('00:10:00', '10 minutes'),
)


class NewGameForm(forms.ModelForm):
    board_size = forms.ChoiceField(choices=BOARD_SIZE)
    main_basic_time = forms.ChoiceField(choices=BASIC_TIME)
    main_byo_yomi = forms.ChoiceField(choices=BYO_YOMI)

    class Meta:
        model = GameInfo
        exclude = ['kifu', 'result', 'white_byo_yomi_periods', 'white_basic_time',
                   'black_basic_time', 'black_byo_yomi_periods', 'comments']
