from django.contrib import admin

# Register your models here.
from games.models import Game, GameInfo, Calling

admin.site.register(Game)
admin.site.register(GameInfo)
admin.site.register(Calling)
