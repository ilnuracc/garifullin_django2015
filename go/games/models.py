import datetime
from django.db import models
from users.models import Profile
from words.models import Comment

GAME_RESULT = (
    ('W', 'White wins'),
    ('B', 'Black wins'),
    ('N', 'Dead heat')
)


# Create your models here.
class GameInfo(models.Model):
    title = models.CharField(max_length=120, default='Friendly match')
    handicap = models.PositiveSmallIntegerField(default=0)
    komi = models.FloatField(default=6.5)
    board_size = models.PositiveSmallIntegerField(default=19)

    main_basic_time = models.TimeField()
    white_basic_time = models.TimeField(default=datetime.datetime.now())
    black_basic_time = models.TimeField(default=datetime.datetime.now())
    main_byo_yomi = models.TimeField()
    main_byo_yomi_periods = models.PositiveSmallIntegerField(default=3)
    white_byo_yomi_periods = models.PositiveSmallIntegerField(default=0)
    black_byo_yomi_periods = models.PositiveSmallIntegerField(default=0)

    kifu = models.TextField(default="(;FF[4])")
    comments = models.ManyToManyField(Comment)
    result = models.CharField(max_length=1, blank=True, choices=GAME_RESULT)

    def __unicode__(self):
        return "%s, %s board size, %s handicap, %s komi, %s basic time, %s byo-yomi" % (
            self.title, self.board_size, self.handicap, self.komi, self.main_basic_time, self.main_byo_yomi
        )


class Game(models.Model):
    game_info = models.ForeignKey(GameInfo)
    white = models.ForeignKey(Profile, related_name='white')
    black = models.ForeignKey(Profile, related_name='black')
    game_date = models.DateTimeField(auto_now=True)
    winner = models.ForeignKey(Profile, related_name='winner', null=True, blank=True)

    def __unicode__(self):
        return "%s between %s(b) and %s(w) | Result: %s" % (
            self.game_info.title, self.black.user.username, self.white.user.username, self.game_info.result
        )


class Calling(models.Model):
    game_info = models.ForeignKey(GameInfo)
    caller = models.ForeignKey(Profile, related_name='caller')
    accepter = models.ForeignKey(Profile, related_name='accepter', null=True, blank=True)
    accepted = models.BooleanField(default=False)

    def __unicode__(self):
        return "from %s" % self.caller.user.username
