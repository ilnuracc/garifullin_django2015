# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0003_auto_20151214_2232'),
    ]

    operations = [
        migrations.AddField(
            model_name='gameinfo',
            name='black_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 14, 22, 38, 8, 173000)),
        ),
        migrations.AddField(
            model_name='gameinfo',
            name='black_byo_yomi_periods',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gameinfo',
            name='white_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 14, 22, 38, 8, 173000)),
        ),
        migrations.AddField(
            model_name='gameinfo',
            name='white_byo_yomi_periods',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
