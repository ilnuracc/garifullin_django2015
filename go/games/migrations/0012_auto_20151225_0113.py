# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0011_auto_20151225_0109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gameinfo',
            name='black_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 25, 1, 13, 38, 132000)),
        ),
        migrations.AlterField(
            model_name='gameinfo',
            name='white_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 25, 1, 13, 38, 132000)),
        ),
    ]
