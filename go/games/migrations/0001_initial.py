# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Calling',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('accepter', models.ForeignKey(related_name='accepter', blank=True, to='users.Profile', null=True)),
                ('caller', models.ForeignKey(related_name='caller', to='users.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game_date', models.DateTimeField(auto_now=True)),
                ('black', models.ForeignKey(related_name='black', to='users.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='GameInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'Friendly match', max_length=120)),
                ('handicap', models.PositiveSmallIntegerField(default=0)),
                ('komi', models.FloatField(default=6.5)),
                ('board_size', models.PositiveSmallIntegerField(default=19)),
                ('basic_time', models.TimeField()),
                ('byo_yomi', models.TimeField()),
                ('byo_yomi_periods', models.PositiveSmallIntegerField(default=3)),
                ('kifu', models.TextField(default=b'(;FF[4])')),
                ('result', models.CharField(blank=True, max_length=1, choices=[(b'W', b'White wins'), (b'B', b'Black wins'), (b'N', b'Dead heat')])),
            ],
        ),
        migrations.AddField(
            model_name='game',
            name='game_info',
            field=models.ForeignKey(to='games.GameInfo'),
        ),
        migrations.AddField(
            model_name='game',
            name='white',
            field=models.ForeignKey(related_name='white', to='users.Profile'),
        ),
        migrations.AddField(
            model_name='game',
            name='winner',
            field=models.ForeignKey(related_name='winner', blank=True, to='users.Profile'),
        ),
        migrations.AddField(
            model_name='calling',
            name='game_info',
            field=models.ForeignKey(to='games.GameInfo'),
        ),
    ]
