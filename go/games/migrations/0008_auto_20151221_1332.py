# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0002_comment'),
        ('games', '0007_auto_20151220_2027'),
    ]

    operations = [
        migrations.AddField(
            model_name='gameinfo',
            name='comments',
            field=models.ManyToManyField(to='words.Comment'),
        ),
        migrations.AlterField(
            model_name='gameinfo',
            name='black_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 21, 13, 32, 22, 27000)),
        ),
        migrations.AlterField(
            model_name='gameinfo',
            name='white_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 21, 13, 32, 22, 27000)),
        ),
    ]
