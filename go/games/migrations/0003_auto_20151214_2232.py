# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0002_auto_20151212_1416'),
    ]

    operations = [
        migrations.RenameField(
            model_name='gameinfo',
            old_name='basic_time',
            new_name='main_basic_time',
        ),
        migrations.RenameField(
            model_name='gameinfo',
            old_name='byo_yomi',
            new_name='main_byo_yomi',
        ),
        migrations.RenameField(
            model_name='gameinfo',
            old_name='byo_yomi_periods',
            new_name='main_byo_yomi_periods',
        ),
    ]
