# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0010_auto_20151224_2233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gameinfo',
            name='black_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 25, 1, 9, 46, 676000)),
        ),
        migrations.AlterField(
            model_name='gameinfo',
            name='white_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 25, 1, 9, 46, 676000)),
        ),
    ]
