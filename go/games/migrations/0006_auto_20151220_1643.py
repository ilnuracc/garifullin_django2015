# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0005_auto_20151214_2343'),
    ]

    operations = [
        migrations.AddField(
            model_name='calling',
            name='accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='gameinfo',
            name='black_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 20, 16, 43, 51, 666000)),
        ),
        migrations.AlterField(
            model_name='gameinfo',
            name='white_basic_time',
            field=models.TimeField(default=datetime.datetime(2015, 12, 20, 16, 43, 51, 666000)),
        ),
    ]
