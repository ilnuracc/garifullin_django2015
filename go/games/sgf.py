import datetime
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from gomill import sgf
import json
from games.models import GameInfo, Game


def adapt_move(x, y, SZ):
    return abs(SZ-1-x), y
    # return abs(SZ-1-y), x     for correct sgf.file


def adapt_move_from_object(obj, SZ):
    return obj[0], adapt_move(obj[1][0], obj[1][1], SZ)


@login_required
def load(request, game_id=None):
    if request.method == 'GET':
        game_info = GameInfo.objects.get(id=game_id)
        game_sgf = sgf.Sgf_game.from_string(game_info.kifu)
        SZ = game_info.board_size

        i = 1
        response = {}
        for node in game_sgf.main_sequence_iter():

            if node.get_move() == (None, None):
                continue
            move = adapt_move_from_object(node.get_move(), SZ)
            json_move = {'color': move[0], 'x': move[1][0], 'y': move[1][1]}
            response[i] = json_move
            i += 1

        return HttpResponse(json.dumps(response))


@login_required
def time(request, game_id=None):
    game_info = GameInfo.objects.get(id=game_id)

    if request.method == 'POST':
        time = json.loads(request.POST['time'])
        if 'white_basic' in time:
            game_info.white_basic_time = time['white_basic']
        if 'black_basic' in time:
            game_info.black_basic_time = time['black_basic']
        if 'white_periods' in time:
            game_info.white_byo_yomi_periods = time['white_periods']
        if 'black_periods' in time:
            game_info.black_byo_yomi_periods = time['black_periods']
        game_info.save()
        return HttpResponse("Ok")
    else:
        response = {'white_basic': game_info.white_basic_time,
                    'black_basic': game_info.black_basic_time,
                    'white_periods': game_info.white_byo_yomi_periods,
                    'black_periods': game_info.black_byo_yomi_periods}
        return HttpResponse(json.dumps(response))



@login_required
def record(request, game_id=None):
    if request.method == 'POST':
        game = Game.objects.get(game_info=GameInfo.objects.get(id=game_id))
        if request.user in (game.black.user, game.white.user):
            obj = json.loads(request.POST['json'])

            game_info = GameInfo.objects.get(id=game_id)
            game_sgf = sgf.Sgf_game.from_string(game_info.kifu)
            new_node = game_sgf.extend_main_sequence()

            new_node.set_move(obj['color'], adapt_move(obj['x'], obj['y'], game_info.board_size))
            game_info.kifu = game_sgf.serialise()

            game_info.save()
            return HttpResponse("Ok")
        return HttpResponse("Forbidden")


def _initiate(game_id):
    game_info = GameInfo.objects.get(id=game_id)

    game_info.black_byo_yomi_periods = game_info.main_byo_yomi_periods
    game_info.white_byo_yomi_periods = game_info.main_byo_yomi_periods
    game_info.black_basic_time = game_info.main_basic_time
    game_info.white_basic_time = game_info.main_basic_time

    game = Game.objects.get(game_info=game_info)
    kifu = game_info.kifu[2:-1]

    kifu += "%s%s%s" % ("GM[", 1, "]")  # refers to GO
    kifu += "%s%s%s" % ("SZ[", game_info.board_size, "]")
    kifu += "%s%s%s" % ("KM[", game_info.komi, "]")
    kifu += "%s%s%s" % ("HA[", game_info.handicap, "]")
    kifu += "%s%s%s" % ("PB[", game.black.rating, "]")
    kifu += "%s%s%s" % ("PW[", game.white.rating, "]")
    kifu += "%s%s%s" % ("BR[", game.black.user.username, "]")
    kifu += "%s%s%s" % ("WR[", game.white.user.username, "]")
    kifu += "%s%s%s" % ("RU[", "Japanese", "]")
    kifu = "%s%s%s" % ("(;", kifu, ")")

    game_info.kifu = kifu
    game_info.save()
