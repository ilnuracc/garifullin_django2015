from django import forms
from core.models import Collection

GENDER = (
    ('M', 'Male'),
    ('F', 'Female'),
    ('N', 'None')
)

PASSWORD = {
    'required': 'This field is required',
    'invalid': 'Enter a valid value'
}


class SettingsForm(forms.Form):
    username = forms.CharField()
    first_name = forms.CharField(required=False)
    current_password = forms.CharField(widget=forms.PasswordInput)
    password = forms.CharField(widget=forms.PasswordInput, required=False)
    repeat_password = forms.CharField(widget=forms.PasswordInput, required=False)
    email = forms.EmailField()
    gender = forms.ChoiceField(choices=GENDER, required=False)
    about_me = forms.CharField(widget=forms.Textarea, required=False)


class UploadForm(forms.ModelForm):
    class Meta:
        model = Collection
        exclude = ['owner']
