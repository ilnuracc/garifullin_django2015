# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='rank',
        ),
        migrations.AddField(
            model_name='profile',
            name='rating',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
