from datetime import datetime
from django.db import models
from django.contrib.auth.models import User


# Create your models here.

GENDER = (
    ('M', 'Male'),
    ('F', 'Female'),
    ('N', 'None')
)


class Profile(models.Model):
    user = models.OneToOneField(User)
    gender = models.CharField(max_length=1, default='N', choices=GENDER)
    about_me = models.TextField(blank=True)
    rating = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return self.user.username
