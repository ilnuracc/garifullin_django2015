from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^(?P<user_id>\d+)/$', 'users.views.profile_page', name='profile_page'),
    url(r'^(?P<user_id>\d+)/games/now$', 'users.views.grid_now', name='now'),
    url(r'^(?P<user_id>\d+)/games/all$', 'users.views.grid_all', name='all'),
    url(r'^settings/$', 'users.views.settings_page', name='settings'),
]
