import json
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist


# Create your views here
from django.utils.safestring import SafeString
from core.models import Collection
from games.models import Game
from users.forms import SettingsForm, UploadForm
from users.models import Profile


@login_required(login_url=reverse_lazy("home:welcome_page"))
def profile_page(request, user_id=None):
    profile = Profile.objects.get(user=User.objects.get(id=user_id))
    if profile.user == request.user:
        own = True
    else:
        own = False
    i = 0
    now = {}
    all = {}

    now_played = Game.objects.exclude(winner__isnull=False).filter(Q(white=profile) | Q(black=profile))
    played = Game.objects.exclude(winner__isnull=True).filter(Q(white=profile) | Q(black=profile))

    for game in now_played.iterator():
        now[i] = {"game_size": game.game_info.board_size,
                  "game_id": game.game_info.id,
                  "now": True,
                  "black": game.black.user.username,
                  "black_rating": game.black.rating,
                  "white": game.white.user.username,
                  "white_rating": game.white.rating}
        i += 1

    i = 0
    for game in played.iterator():
        all[i] = {"game_size": game.game_info.board_size,
                  "game_id": game.game_info.id,
                  "now": False,
                  "black": game.black.user.username,
                  "black_rating": game.black.rating,
                  "white": game.white.user.username,
                  "white_rating": game.white.rating}
        i += 1

    # if own profile
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            game = Collection(file=request.FILES['file'],
                              comment=form.cleaned_data['comment'],
                              owner=Profile.objects.get(user=request.user))
            game.save()
            return HttpResponseRedirect(reverse_lazy('profile:profile_page', args=[request.user.id]))
        else:
            return render(request, 'users/profile.html', {'headline': profile.user.username + ' profile',
                                                          'own': own,
                                                          'user_id': user_id,
                                                          'profile': profile,
                                                          'form': form})
    elif request.method == "GET":
        form = UploadForm()
        collection = Collection.objects.filter(owner=Profile.objects.get(id=user_id))
        return render(request, "users/profile.html", {"headline": profile.user.username + " profile",
                                                      "own": own,
                                                      'user_id': user_id,
                                                      'profile': profile,
                                                      'collection': collection,
                                                      'form': form,
                                                      "now": SafeString(json.dumps(now)),
                                                      "all": SafeString(json.dumps(all))})


    # if somebody's
    else:
        try:
            return render(request, 'users/profile.html', {'headline': profile.user.username + ' profile',
                                                          'own': own,
                                                          'user_id': user_id,
                                                          'profile': profile,
                                                          "now": SafeString(json.dumps(now)),
                                                          "all": SafeString(json.dumps(all))})
        except ObjectDoesNotExist:
            return render(request, 'blank.html', {'headline': '404 profile',
                                                  'message': 'Sorry, the profile does not exist.'})


@login_required(login_url=reverse_lazy("home:welcome_page"))
def settings_page(request):
    profile = Profile.objects.get(user=request.user)

    if request.method == 'POST':

        form = SettingsForm(request.POST)
        if form.is_valid():

            user = authenticate(
                username=request.user.username,
                password=form.cleaned_data['current_password']
            )

            if user:

                user = User.objects.get(id=request.user.id)
                profile = Profile.objects.get(user=request.user)

                user.username = form.cleaned_data['username']
                user.first_name = form.cleaned_data['first_name']

                if form.cleaned_data['password'] == form.cleaned_data['repeat_password'] \
                        and form.cleaned_data['password']:
                    user.set_password(form.cleaned_data['password'])
                    user.save()
                    reloginned = authenticate(username=profile.user.username, password=form.cleaned_data['password'])
                    login(request, reloginned)

                elif form.cleaned_data['password'] != form.cleaned_data['repeat_password'] \
                        and form.cleaned_data['password'] and form.cleaned_data['repeat_password']:
                    return render(request, 'users/settings.html', {'headline': profile.user.username + ' profile',
                                                                   'form': form,
                                                                   'not_match': 'Passwords doesn\'t match!'})

                user.email = form.cleaned_data['email']
                profile.gender = form.cleaned_data['gender']
                profile.about_me = form.cleaned_data['about_me']

                user.save()
                profile.save()
                return render(request, 'users/settings.html', {'headline': profile.user.username + ' profile',
                                                               'form': form})
            else:
                return render(request, 'users/settings.html', {'headline': profile.user.username + ' profile',
                                                               'form': form,
                                                               'wrong_pass': 'Wrong password!'})
        else:
            return render(request, 'users/settings.html', {'headline': profile.user.username + ' profile',
                                                           'form': form})

    else:
        form_data = {
            'username': request.user.username,
            'first_name': first_name_field(request.user),
            'current_password': True,
            'password': True,
            'repeat_password': True,
            'email': request.user.email,
            'gender': gender_field(request.user.profile),
            'about_me': about_me_field(request.user.profile)
        }
        form = SettingsForm(form_data)

    return render(request, 'users/settings.html', {'headline': profile.user.username + " profile",
                                                   'form': form})


@login_required(login_url=reverse_lazy("home:welcome_page"))
def grid_now(request, user_id=None):
    profile = Profile.objects.get(user=User.objects.get(id=user_id))

    now_played = Game.objects.exclude(winner__isnull=False).filter(Q(white=profile) | Q(black=profile))

    i = 0
    response = {}

    for game in now_played.iterator():
        response[i] = {"game_size": game.game_info.board_size,
                       "game_id": game.game_info.id,
                       "now": True,
                       "black": game.black.user.username,
                       "black_rating": game.black.rating,
                       "white": game.white.user.username,
                       "white_rating": game.white.rating}
        i += 1

    return render(request, "games/grid.html", {"headline": profile.user.username + " played now",
                                               "json": SafeString(json.dumps(response))})


@login_required(login_url=reverse_lazy("home:welcome_page"))
def grid_all(request, user_id=None):
    profile = Profile.objects.get(user=User.objects.get(id=user_id))

    all_played = Game.objects.exclude(winner__isnull=True).filter(Q(white=profile) | Q(black=profile))

    i = 0
    response = {}

    for game in all_played.iterator():
        response[i] = {"game_size": game.game_info.board_size,
                       "game_id": game.game_info.id,
                       "now": True,
                       "black": game.black.user.username,
                       "black_rating": game.black.rating,
                       "white": game.white.user.username,
                       "white_rating": game.white.rating}
        i += 1

    return render(request, "games/grid.html", {"headline": profile.user.username + " played",
                                               "json": SafeString(json.dumps(response))})


def first_name_field(user):
    if user.first_name:
        return user.first_name
    return ' '


def gender_field(profile):
    if profile.gender:
        return profile.gender
    return ' '


def about_me_field(profile):
    if profile.about_me:
        return profile.about_me
    return ' '
