
    var PAGE = 0;
    var th = new Array();

    function GameThumbnail(id, desk, ctx, background, game_size, grid_space) {
        this.game_id = id;
        this.desk = desk;
        this.ctx = ctx;
        this.background = background;
        this.game_size = game_size;

        this.measurements = [];
        this.k = 0;
        this.stars = [];
        this.grid_space = grid_space;
        this.stone_size = 0;
        this.dots_radius = 0;

        this.kifu = new Array(game_size);
        this.tree = new Tree();
        var self = this;

        this.initiate = function () {
            if (game_size == 9)
                self.stone_size = 9;
            else if (game_size == 13)
                self.stone_size = 13;
            else if (game_size == 19)
                self.stone_size = 5;

            self.k = Math.round((self.desk.width-self.grid_space)/self.game_size);
            for (var i = self.grid_space; i <= self.desk.width-self.grid_space; i += self.k)
                self.measurements.push(i);   // fill Measurements

            for (i = 0; i < self.kifu.length; i++) {
                self.kifu[i] = new Array(self.game_size);
                for (var j = 0; j < self.kifu[i].length; j++)
                    self.kifu[i][j] = new Stone("N");
            }

            for (i in self.stars)
                for (j in self.stars[i])
                    self.stars[i][j] = self.grid_space + self.stars[i][j] * self.K;

            self._load();
        };

        this._load = function () {
            $.ajax({
                url: '/games/' + self.game_id + '/load',
                method: 'GET',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
                },
                success: function(data){
                    data = JSON.parse(data);
                    for (var key in data) {
                        self.kifu[data[key].x][data[key].y].color = data[key].color.toUpperCase();
                        self.tree.addNode(data[key].color.toUpperCase(), data[key].x, data[key].y);
                        //var captured_group = isSomeCaptured();
                        //if (captured_group)
                        //    capture(captured_group);
                    }
                    drawExistStones(self.ctx, self.desk, self.kifu, self.stars,
                                    self.game_size, self.measurements, self.stone_size,
                                    self.background, self.grid_space, self.dots_radius);
                },
                error: function(data, textStatus){
                    alert(textStatus.toUpperCase() + ": " + data);
                }
            });
        };
    }

    function show(height, width, amount, json, id_pfx, grid_space) {
        json = JSON.parse(json);
        var json_l = Object.keys(json).length;

        for (var i = PAGE, j = 1; i < json_l && i < PAGE*amount, j <= amount; i++, j++) {
            if (json[i]) {
                var obj = json[i];
                var elem = document.getElementById(id_pfx + j);

                var canvas = document.createElement("canvas");
                canvas.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.classList.add('th-hover');

                var players = document.createElement("div");
                var black = document.createElement("span");
                var white = document.createElement("span");
                black.innerHTML = obj.black + " (" + obj.black_rating + ")";
                white.innerHTML = obj.white + " (" + obj.white_rating + ")";
                black.classList.add("u-pull-left");
                white.classList.add("u-pull-right");

                players.appendChild(black);
                players.appendChild(white);
                elem.appendChild(players);
                elem.appendChild(canvas);

                var clickCallback = function(index){
                    return function (){
                         window.document.location = '/games/' + index;
                    };
                };
                elem.addEventListener('click', clickCallback(obj.game_id));

                th.push(new GameThumbnail(obj.game_id,
                    canvas,
                    canvas.getContext('2d'),
                    background,
                    obj.game_size,
                    grid_space
                ));
            }
        }

        background.onload = function() {
            for (var i = 0; i < th.length; i++) {
                th[i].initiate();
            }
        };
    }

    var bg = document.getElementById("bg");
    var background = new Image();
        background.src = bg.src;