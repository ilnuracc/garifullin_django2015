

    var update_timer = setTimeout(function listenToCallUpdates() {
        $.ajax({
            url: '/callings/check',
            method: 'GET',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            success: function(data){
                data = JSON.parse(data);

                if (Object.keys(data).length) {

                    for (var item in data) {

                        if (data[item] == window.location.pathname)
                            break;

                        var a = document.createElement('a');
                        a.innerHTML = 'accepted';
                        a.setAttribute('href', data[item]);
                        a.addEventListener('click', function() {
                            var splitted = this.getAttribute('href').split('/');
                            $.ajax({
                                url: '/callings/recd',
                                method: 'POST',
                                beforeSend: function(xhr) {
                                    xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
                                },
                                data: {
                                    'game_id': splitted[splitted.length-1]
                                },
                                error: function(data, textStatus){
                                    alert(textStatus.toUpperCase() + ": " + data);
                                }
                            });
                        });

                        var note = document.createElement("div");
                        note.setAttribute('id', 'note');
                        note.innerHTML = 'Your call is ';
                        note.style.fontWeight = '500';

                        var close = document.createElement("a");
                        close.setAttribute('id', 'close');
                        close.innerHTML = ' <i class="fa fa-times-circle"></i>';
                        close.style.color = '#52616b';

                        close.addEventListener('click', function() {
                            note.style.display = 'none';
                        }, false);


                        note.appendChild(a);
                        note.appendChild(close);

                        document.body.appendChild(note);
                    }

                } else
                    update_timer = setTimeout(listenToCallUpdates, 6000);
            }
        });
    }, 6000);


