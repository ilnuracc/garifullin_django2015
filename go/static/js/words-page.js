
    var WORDS_l = words;
    var form_button = document.getElementById("comment-form");

    form_button.addEventListener('click', function () {
        var value = document.getElementById("id_text").value;
        if (value && !/^\s*$/.test(value)) {
            $.ajax({
                url: '/words/' + room_id,
                method: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
                },
                data: {
                    "text": value
                },
                success: function (data) {
                    WORDS_l++;
                    json = JSON.parse(data);
                    document.getElementById("id_text").value = "";
                    createComment(json[0])
                },
                error: function (data, textStatus) {
                    alert(textStatus.toUpperCase() + ": " + data);
                }
            });
        }
    });

    var words_update_t = setTimeout(function words_update_f() {
        $.ajax({
            url: '/words/' + room_id,
            method: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            success: function (data) {
                json = JSON.parse(data);
                var new_w_l = Object.keys(json).length;
                if (new_w_l > WORDS_l) {
                    for (var i = WORDS_l; i < new_w_l; i++)
                        createComment(json[i]);
                    WORDS_l = new_w_l;
                }
                words_update_t = setTimeout(words_update_f, 5000);
            },
            error: function (data, textStatus) {
                alert(textStatus.toUpperCase() + ": " + data);
            }
        })
    }, 5000);

    function createComment(elem) {
        var window = document.getElementById("window");
        var c = document.createElement("p");

        var a = document.createElement("a");
        var br = document.createElement("br");
        var text = document.createTextNode(elem['text']);

        a.setAttribute('href', '#');
        a.style.textDecoration = 'none';
        a.style.fontWeight = 500;
        if (elem['me'])
            a.style.fontWeight = 800;
        a.innerHTML = elem['sender'] + "&nbsp;" + elem['pub_date'];

        c.appendChild(a);
        c.appendChild(br);
        c.appendChild(text);

        window.appendChild(c);
        window.scrollTop = window.scrollHeight - window.clientHeight;
    }

