/**
 * Created by Ilnur on 12.12.2015.
 */

function Tree() {
    this._moves = 0;
    this._nodes = {
        0: {
            i: null,
            j: null,
            color: null,
            _parent: parent,
            _next: null,
            setNext: function(node) {
                this._next = node;
            },
            getChild: function(node) {
                return this._next;
            }
        }
    };
    var self = this;

    this.getRoot = function() { return self._nodes[0]; };

    this.getLastNode = function() {
        return self._nodes[self._moves-1];
    };

    this.getNode = function (index) {
        return self._nodes[index];
    };

    this.nextNode = function(node) {
        return node.getChild();
    };

    this.addNode = function(color, i, j) {
        if (self._moves == 0) {
            self.getRoot().color = color;
            self.getRoot().i = i;
            self.getRoot().j = j;
            self._moves++;
            return;
        }

        self.getLastNode().setNext(new Node(self.getLastNode(), color, i, j));
        self._nodes[self._moves] = self.getLastNode().getChild();
        self._moves++;
    };

    this.undoAdding = function() {
        self._moves--;
        self.getLastNode().setNext(null);
        delete self._nodes[self._moves+1]
    };

    this.getMoves = function() {
        return self._moves;
    }
}


function Node(parent, color, i, j) {
    this.i = i;
    this.j = j;
    this.color = color;
    this._parent = parent;
    this._next = null;

    var self = this;

    this.getParent = function() {
        return self._parent;
    };

    this.getChild = function() {
        return self._next;
    };

    this.setNext = function (node) {
        self._next = node;
    };

    this.setParent = function (node) {
        self._parent = node;
    };
}


function Stone(color) {
    this.flag = 0;
    this.color = color;
}


function Ko(positions) {
    this.x = positions[0];
    this.y = positions[1];
    this.counter = 0;
}


function GameTime(id_basic, id_byo_yomi, id_periods, hours, minutes, seconds, byo_yomi_m, byo_yomi_s, periods) {
    this._null_time = new Date().getTime();
    this._main_time = new Date().setTime(this._null_time +
            1000 * 3600 * hours + 1000 * 60 * minutes + 1000 * seconds);
    this._byo_yomi = new Date().setTime(this._null_time +
            1000 * 60 * byo_yomi_m + 1000 * byo_yomi_s);
    this._periods = periods;

    var self = this;
    var bym = byo_yomi_m;
    var bys = byo_yomi_s;

    this.timer = 0;
    this.id_basic = id_basic;
    this.id_byo_yomi = id_byo_yomi;
    this.id_periods = id_periods;

    this.getMainTime = function() {
        return self._main_time;
    };

    this._getTimer = function () {
        return self.timer;
    };

    this._tick = function() {
        if (self._null_time != self._main_time) {
            self._main_time = new Date().setTime(self._main_time-1000);
            date = new Date(self._main_time-self._null_time);
            document.getElementById(self.id_basic).innerHTML = (date.getHours()) + ":" + date.getMinutes() + ":" + date.getSeconds();
        } else if (self._periods != 0) {
            self._byo_yomi = new Date().setTime(self._byo_yomi-1000);
            document.getElementById(self.id_byo_yomi).innerHTML = date.getMinutes() + ":" + date.getSeconds();
        } else
            return false;
        return true;
    };

    this.start = function () {
        self.timer = setInterval(function run() {
            if (self._tick()) {
                if (self._null_time == self._byo_yomi) {
                    self._periods--;
                    self._byo_yomi = new Date().setTime(self._null_time +
                            1000 * 60 * bym + 1000 * bys);
                    document.getElementById(self.id_periods).innerHTML = self._periods;
                }
            } else
                console.log('You lose!');
        }, 1000);
    };

    this.stop = function (timer) {
        clearTimeout(timer);
        self._byo_yomi = new Date().setTime(self._null_time +
                            1000 * 60 * bym + 1000 * bys);
    };
}



