
    var COMMENTS_l = comments;

    function snapToGrid() {
        var boardCoord = desk.getBoundingClientRect();
        current_x = clearCoordinate(event.clientX, boardCoord.left);
        current_y = clearCoordinate(event.clientY, boardCoord.top);
        for (var i = 0; i < GAME_SIZE; i++) {
            if (current_x == MEASUREMENTS[i])
                target_i = i;
            if (current_y == MEASUREMENTS[i])
                target_j = i;
        }
    }


    function clearCoordinate(k, offset) {
        k -= offset;
        console.log(k);
        var difference = 1000;
        var point = 0;
        for (var i in MEASUREMENTS)
            if (Math.abs(k-MEASUREMENTS[i]) < difference) {
                difference = Math.abs(k-MEASUREMENTS[i]);
                point = MEASUREMENTS[i];
            }
        return point;
    }


    function defineColor(str) {
        if (str == 'W')
            return 'white';
        if (str == 'B')
            return 'black';

        if (str == 'white')
            return 'W';
        if (str == 'black')
            return 'B';
    }


    function swap(color) {
        if (color == 'B')
            return 'W';
        if (color == 'black')
            return 'white';
        if (color == 'W')
            return 'B';
        if (color == 'white')
            return 'black';
    }


    function clearBoard() {
        var N = 0;
        for (var i = 0; i < GAME_SIZE; i++)
            for (var j = 0; j < GAME_SIZE; j++)
                if (kifu[i][j].flag == -1) {
                    kifu[i][j] = new Stone('N');
                    N++;
                }
        return N;
    }


    function addStonesFromDB(data) {
        var i = 0;
        for (var key in data) {
            target_i = data[key].x;
            target_j = data[key].y;
            kifu[data[key].x][data[key].y].color = data[key].color.toUpperCase();
            tree.addNode(data[key].color.toUpperCase(), data[key].x, data[key].y);
            var captured_group = isSomeCaptured();
            if (captured_group)
                capture(captured_group);
            i++;
        }

        if (i) {
            current_x = MEASUREMENTS[data[i].x];
            current_y = MEASUREMENTS[data[i].y];
            last_target_i = data[i].x;
            last_target_j = data[i].y;
        }
    }


    function writeToKifu() {
        copy_last_target_i = last_target_i;
        copy_last_target_j = last_target_j;
        last_target_i = target_i;
        last_target_j = target_j;

        if (tree.getLastNode() && tree.getLastNode().color == 'B') {
            kifu[target_i][target_j].color = 'W';
            tree.addNode('W', target_i, target_j);
        } else {
            kifu[target_i][target_j].color = 'B';
            tree.addNode('B', target_i, target_j);
        }

        ko.counter++;
        console.log(kifu[target_i][target_j].color + ' on ' + (target_i+1) + ':' + (target_j+1));
    }


    function unwriteKifu() {
        console.log('You can\'t do this move!');

        tree.undoAdding();
        kifu[target_i][target_j] = new Stone('N');
        ko.counter--;

        target_i = last_target_i;
        target_j = last_target_j;
        last_target_i = copy_last_target_i;
        last_target_j = copy_last_target_j;
    }


    function definePlayer() {
        if (black == user)
            player = 'B';
        else if (white == user)
            player = 'W';
        else
            player = 'O'
    }


    function listenUpdates() {
        var listening_timer = setTimeout(function findDifference(){
            $.ajax({
                url: '/games/' + game_id + '/load',
                method: 'GET',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
                },
                success: function(data){
                    data = JSON.parse(data);

                    var new_key = tree.getMoves()+1;
                    if (data[new_key]) {

                        target_i = data[new_key].x;
                        target_j = data[new_key].y;
                        copy_last_target_i = last_target_i;
                        copy_last_target_j = last_target_j;
                        last_target_i = target_i;
                        last_target_j = target_j;
                        current_x = MEASUREMENTS[data[new_key].x];
                        current_y = MEASUREMENTS[data[new_key].y];

                        kifu[data[new_key].x][data[new_key].y].color = data[new_key].color.toUpperCase();
                        tree.addNode(data[new_key].color.toUpperCase(), data[new_key].x, data[new_key].y);

                        var captured_group = isSomeCaptured();
                        if (captured_group)
                            capture(captured_group);

                        drawPlayedStone(captured_group);
                        // opponent_time.stop();
                        // own_time.start();
                        // syncTime();
                    } else {
                        listening_timer = setTimeout(findDifference, LISTENING_INTERVAL);
                    }
                },
                error: function(data, textStatus){
                    alert(textStatus.toUpperCase() + ": " + data);
                }
            });
        }, LISTENING_INTERVAL)
    }

    function syncTime() {
        $.ajax({
            url: '/games/' + game_id + '/time',
            method: 'GET',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            success: function(data){
                data = JSON.parse(data);

                if (player == 'B') {
                    obj = data['white_basic'].split(":");
                    hours_white = obj[0];
                    minutes_white = obj[1];
                    seconds_white = obj[2];
                    periods_white = data['white_periods'];
                } else {
                    obj = data['black_basic'].split(":");
                    hours_black = obj[0];
                    minutes_black = obj[1];
                    seconds_black = obj[2];
                    periods_black = data['black_periods'];
                }

            },
            error: function(data, textStatus){
                alert(textStatus.toUpperCase() + ": " + data);
            }
        });
    }

    function formatTime(time, time_sec, time_th) {
        if (arguments.length == 3) {
            var response = '';
            if (time.toString().length == 1) { response += '0' + time; }
            else { response += time; }
            if (time_sec.toString().length == 1) { response += ':0' + time_sec; }
            else { response += ':' + time_sec; }
            if (time_th.toString().length == 1) { response += ':0' + time_th; }
            else { response += ':' + time_th; }
            return response;
        } else if (time.toString().length == 1)
            return 0 + time;
        return time;
    }


    function possible() {
        if (tree.getLastNode() && tree.getLastNode().color != player || !tree.getLastNode() && player == 'B')
            return POSSIBLE;
    }

    var form_button = document.getElementById("comment-form");

    form_button.addEventListener('click', function () {
        var value = document.getElementById("id_text").value;
        if (value && !/^\s*$/.test(value)) {
            $.ajax({
                url: '/games/' + game_id + '/comments',
                method: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
                },
                data: {
                    "text": value
                },
                success: function (data) {
                    COMMENTS_l++;
                    json = JSON.parse(data);
                    document.getElementById("id_text").value = "";
                    createComment(json[Object.keys(json).length - 1])
                },
                error: function (data, textStatus) {
                    alert(textStatus.toUpperCase() + ": " + data);
                }
            });
        }
    });

    var comments_update_t = setTimeout(function comments_update_f() {
        $.ajax({
            url: '/games/' + game_id + '/comments',
            method: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            data: {
                "text": document.getElementById("id_text").value
            },
            success: function (data) {
                json = JSON.parse(data);
                new_c_l = Object.keys(json).length;
                if (new_c_l > COMMENTS_l) {
                    for (var i = COMMENTS_l; i < new_c_l; i++)
                        createComment(json[i]);
                    COMMENTS_l = new_c_l;
                }
                comments_update_t = setTimeout(comments_update_f, 4000);
            },
            error: function (data, textStatus) {
                alert(textStatus.toUpperCase() + ": " + data);
            }
        })
    }, 4000);

    function createComment(elem) {
        var comments = document.getElementById("comments");
        var c = document.createElement("p");

        //var span = document.createElement("span");
        var a = document.createElement("a");
        var br = document.createElement("br");
        //var t = document.createTextNode(">>> ");
        var text = document.createTextNode(elem['text']);

        //span.style.fontStyle = 'italic';
        //span.innerHTML = elem['pub_date'] + "&nbsp;&nbsp;";
        a.setAttribute('href', '#');
        a.style.textDecoration = 'none';
        if (elem['me'])
            a.style.fontWeight = 800;
        a.innerHTML = elem['sender'] + "&nbsp;" + elem['pub_date'];

        //c.appendChild(span);
        c.appendChild(a);
        c.appendChild(br);
        //c.appendChild(t);
        c.appendChild(text);

        comments.insertBefore(c, comments.firstElementChild);
    }