

    /* MAIN CONSTANTS */
    var MEASUREMENTS = [];
    var STONE_SIZE = 15;
    var GAME_SIZE = 19;
    var GRID_SPACE = 30.5;
    var DOTS_RADIUS = 3;
    var STARS_POSITIONS = [[3, 3], [3, 9], [3, 15], [9, 3], [9, 9], [9, 15], [15, 3], [15,9], [15, 15]];
    var K;
    var POSSIBLE = true;
    var LISTENING_INTERVAL = 3000;    // ms


    /* MAIN VARIABLES */
    var kifu = new Array(GAME_SIZE);
    var tree = new Tree();
    var ko = new Ko([-1, -1]);
    var player = '';
    var listening_timer;
    var own_time = '';
    var opponent_time = '';

    // Show current cleared stone position relatively the board
    var current_x = 0;
    var current_y = 0;

    // Show position in matrix i == x; j == y
    var target_i = 0;
    var target_j = 0;
    var last_target_i = 0, copy_last_target_i = 0;
    var last_target_j = 0, copy_last_target_j = 0;



    /* DOM VARIABLES */
    var desk = document.getElementById("desk");
    var ctx = desk.getContext('2d');

    var bg = document.getElementById("bg");
    var background = new Image();
        background.src = bg.src;

    background.onload = function() {
        initiate();
    };



    /* START FUNCTIONS */
    function initiate() {
        definePlayer();

        K = Math.round((desk.width-GRID_SPACE)/GAME_SIZE);   // constant
        for (var i = GRID_SPACE; i <= desk.width-GRID_SPACE; i += K)
            MEASUREMENTS.push(i);   // fill Measurements

        for (i = 0; i < kifu.length; i++) {
            kifu[i] = new Array(GAME_SIZE);
            for (var j = 0; j < kifu[i].length; j++)
                kifu[i][j] = new Stone("N");
        }

        // if (player == 'B') {
        //     own_time = new GameTime('basic_time_black', 'byo_yomi_black', 'periods_black', hours_black, minutes_black, seconds_black, byo_yomi_m, byo_yomi_s, periods_black);
        //     opponent_time = new GameTime('basic_time_white', 'byo_yomi_white', 'periods_white', hours_white, minutes_white, seconds_white, byo_yomi_m, byo_yomi_s, periods_white);
        // } else {
        //     own_time = new GameTime('basic_time_white', 'byo_yomi_white', 'periods_white', hours_white, minutes_white, seconds_white, byo_yomi_m, byo_yomi_s, periods_white);
        //     opponent_time = new GameTime('basic_time_black', 'byo_yomi_black', 'periods_black', hours_black, minutes_black, seconds_black, byo_yomi_m, byo_yomi_s, periods_black);
        // }

        // stars correcting
        for (i in STARS_POSITIONS)
            for (j in STARS_POSITIONS[i])
                STARS_POSITIONS[i][j] = GRID_SPACE + STARS_POSITIONS[i][j] * K;

        $("#headline").replaceWith("<h3 id=\"headline\" style=\"margin: 50px 0 -10px 8.8%\">" + title + "</h3>");
        document.getElementById("main-hr").style.visibility = "hidden";

        document.getElementById("basic_time_black").innerHTML = hours_black + ":" + minutes_black + ":" + seconds_black;
        document.getElementById("basic_time_white").innerHTML = hours_white + ":" + minutes_white + ":" + seconds_white;

        $.ajax({
            url: '/games/' + game_id + '/load',
            method: 'GET',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            success: function(data){
                data = JSON.parse(data);
                addStonesFromDB(data);
                drawExistStones(ctx, desk, kifu, STARS_POSITIONS, GAME_SIZE, MEASUREMENTS, STONE_SIZE, background, GRID_SPACE, DOTS_RADIUS);
                if (tree.getMoves()) { drawCurrentStoneMark(true); }
                if (possible()) {
                    listenUpdates();
                }
                    // own_time.start();
                // else {
                    // opponent_time.start();
                // }
            },
            error: function(data, textStatus){
                alert(textStatus.toUpperCase() + ": " + data);
            }
        });

        $.ajax({
            url: '/games/' + game_id + '/status',
            method: 'GET',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            success: function(response){
                var json = JSON.parse(response);
                if (json['0'] == false) {
                    document.getElementById("game-actions").style.visibility = 'hidden';
                    POSSIBLE = false;
                    if (json['1'] == 'B') {
                        document.getElementById("basic_time_black").innerHTML = 'Winner';
                        document.getElementById("basic_time_white").style.visibility = 'hidden';
                        document.getElementById("basic_time_white").style.height = 0;
                        document.getElementById("white_b").style.visibility = 'hidden';
                        document.getElementById("black_b").style.visibility = 'hidden';
                    } else {
                        document.getElementById("basic_time_white").innerHTML = 'Winner';
                        document.getElementById("basic_time_black").style.visibility = 'hidden';
                        document.getElementById("basic_time_black").style.height = 0;
                        document.getElementById("white_b").style.visibility = 'hidden';
                        document.getElementById("black_b").style.visibility = 'hidden';
                    }
                }
            }
        });


    }



    /* MAIN LOGIC */
    desk.onclick = function() {
        // if (!tree.getLastNode() && player == 'B')
            // own_time.start();

        if (possible()) {
            snapToGrid();
            if (isPlayed())
                return;

            writeToKifu();

            var captured_group = isSomeCaptured();
            var is_ko = false;
            if (captured_group)
                is_ko = isKo(captured_group);

            if (captured_group && !is_ko) {
                capture(captured_group);
                drawExistStones(ctx, desk, kifu, STARS_POSITIONS, GAME_SIZE, MEASUREMENTS, STONE_SIZE, background, GRID_SPACE, DOTS_RADIUS);
            } else if (isSuicide(captured_group) || is_ko) {
                if (is_ko)
                    console.log('Ko!');
                else
                    console.log('Suicide!');
                unwriteKifu();
                return;
            }


            /* RECORD MOVE  +ajax */
            var move = {
                color: kifu[target_i][target_j].color.toLowerCase(),
                x: target_i,
                y: target_j
            };

            $.ajax({
                    url: '/games/' + game_id + '/move',
                    method: 'POST',
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
                    },
                    data: {
                        "json": JSON.stringify(move)
                    },
                    success: function(data){
                        if (data == 'Forbidden') {
                            alert(data);
                        }
                    },
                    error: function(data, textStatus){
                        alert(textStatus.toUpperCase() + ": " + data);
                    }
                }
            );

            // var time = {};
            // if (player == 'B') {
            //     time['black_basic'] = formatTime(hours_black, minutes_black, seconds_black);
            //     time['black_periods'] = periods_black;
            // } else {
            //     time['white_basic'] = formatTime(hours_white, minutes_white, seconds_white);
            //     time['white_periods'] = periods_white;
            // }

            // $.ajax({
            //     url: '/games/' + game_id + '/time',
            //     method: 'POST',
            //     beforeSend: function(xhr) {
            //             xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            //         },
            //         data: {
            //             "time": JSON.stringify(time)
            //         },
            //         success: function(data){
            //             if (data == 'Forbidden') {
            //                 alert(data);
            //             }
            //         },
            //         error: function(data, textStatus){
            //             alert(textStatus.toUpperCase() + ": " + data);
            //         }
            // });

            // own_time.stop(own_time.timer);
            // opponent_time.start();
            drawPlayedStone(captured_group);
            listenUpdates();
        } else {
            console.log('There is not your move!');
        }
    };


    function isSomeCaptured() {
        var color = swap(tree.getLastNode().color);
        var bool = false;
        var positions = [[0, 0], [0, 0], [0, 0], [0, 0]];
        var liberties = [0, 0, 0, 0];
        var colors = ['', '', '', ''];
        var flags = [0, 0, 0, 0];

        if (isOnBoard(target_i-1, target_j) && kifu[target_i-1][target_j].color == color && !kifu[target_i-1][target_j].flag)
        {
            liberties[0] += deep(target_i-1, target_j, color, 1);
            colors[0] = kifu[target_i-1][target_j].color;
            flags[0] = kifu[target_i-1][target_j].flag;
            positions[0] = [target_i-1, target_j];

        }


        if (isOnBoard(target_i+1, target_j) && kifu[target_i+1][target_j].color == color && !kifu[target_i+1][target_j].flag)
        {
            liberties[1] += deep(target_i+1, target_j, color, 2);
            colors[1] = kifu[target_i+1][target_j].color;
            flags[1] = kifu[target_i+1][target_j].flag;
            positions[1] = [target_i+1, target_j];
        }


        if (isOnBoard(target_i, target_j-1) && kifu[target_i][target_j-1].color == color && !kifu[target_i][target_j-1].flag)
        {
            liberties[2] += deep(target_i, target_j-1, color, 3);
            colors[2] = kifu[target_i][target_j-1].color;
            flags[2] = kifu[target_i][target_j-1].flag;
            positions[2] = [target_i, target_j-1];
        }


        if (isOnBoard(target_i, target_j+1) && kifu[target_i][target_j+1].color == color && !kifu[target_i][target_j+1].flag)
        {
            liberties[3] += deep(target_i, target_j+1, color, 4);
            colors[3] = kifu[target_i][target_j+1].color;
            flags[3] = kifu[target_i][target_j+1].flag;
            positions[3] = [target_i, target_j+1];
        }

        // console.log('---');
        // console.log('left: ' + liberties[0] + ' flags: ' + flags[0]);
        // console.log('right: ' + liberties[1] + ' flags: ' + flags[1]);
        // console.log('up: ' + liberties[2] + ' flags: ' + flags[2]);
        // console.log('down: ' + liberties[3] + ' flags: ' + flags[3]);
        // console.log('---')

        restateFlags(target_i, target_j, color);

        // return captured group position
        return function check() {
            var bool = false;
            var resp = [];
            for (var stone in flags)
                if (flags[stone] != 0 && colors[stone] == swap(tree.getLastNode().color) && liberties[stone] == 0) {
                    bool = true;
                    resp.push(positions[stone]);
                }
            if (bool)
                return resp;
        }();
    }


    function restateFlags(i, j, color) {
        kifu[i][j].flag = 0;
        if (isOnBoard(i-1, j) && (kifu[i-1][j].color == color || kifu[i-1][j].color == 'N') && kifu[i-1][j].flag)
            restateFlags(i-1, j, color);
        if (isOnBoard(i+1, j) && (kifu[i+1][j].color == color || kifu[i+1][j].color == 'N') && kifu[i+1][j].flag)
            restateFlags(i+1, j, color);
        if (isOnBoard(i, j-1) && (kifu[i][j-1].color == color || kifu[i][j-1].color == 'N') && kifu[i][j-1].flag)
            restateFlags(i, j-1, color);
        if (isOnBoard(i, j+1) && (kifu[i][j+1].color == color || kifu[i][j+1].color == 'N') && kifu[i][j+1].flag)
            restateFlags(i, j+1, color);
    }


    function capture(positions) {
        if (positions.length == 1)
            last_captured = [positions[0][0], positions[0][1]];


        var color = swap(tree.getLastNode().color);
        for (var i in positions)
            deep(positions[i][0], positions[i][1], color, -1);
        clearBoard();

        console.log(positions[0][0] + ":" + positions[0][1] + ' group is captured!');
    }


    function isKo(captured_group) {
        var bool = false;
        if (captured_group.length == 1)
            if (ko.x == target_i && ko.y == target_j && ko.counter < 2)
                 bool = true;
        ko = new Ko(captured_group[0]);
        return bool;
    }


    function deep(i, j, color, flag) {
        var N = 0;
        kifu[i][j].flag = flag;
        if (isOnBoard(i-1, j) && kifu[i-1][j].color == color && !kifu[i-1][j].flag)
            N += deep(i-1, j, color, flag);
        if (isOnBoard(i+1, j) && kifu[i+1][j].color == color && !kifu[i+1][j].flag)
            N += deep(i+1, j, color, flag);
        if ( isOnBoard(i, j-1) && kifu[i][j-1].color == color && !kifu[i][j-1].flag)
            N += deep(i, j-1, color, flag);
        if ( isOnBoard(i, j+1) && kifu[i][j+1].color == color && !kifu[i][j+1].flag)
            N += deep(i, j+1, color, flag);

        if (isOnBoard(i-1, j) && kifu[i-1][j].color == 'N' && (!kifu[i-1][j].flag || kifu[i-1][j].flag != flag))
            { N++; kifu[i-1][j].flag = flag; }
        if (isOnBoard(i+1, j) && kifu[i+1][j].color == 'N' && (!kifu[i+1][j].flag || kifu[i+1][j].flag != flag))
            { N++; kifu[i+1][j].flag = flag; }
        if (isOnBoard(i, j-1) && kifu[i][j-1].color == 'N' && (!kifu[i][j-1].flag || kifu[i][j-1].flag != flag))
            { N++; kifu[i][j-1].flag = flag; }
        if (isOnBoard(i, j+1) && kifu[i][j+1].color == 'N' && (!kifu[i][j+1].flag || kifu[i][j+1].flag != flag))
            { N++; kifu[i][j+1].flag = flag; }

        return N;
    }


    function isOnBoard(i, j) {
        return !!(i >= 0 && j >= 0 && i < GAME_SIZE && j < GAME_SIZE);
    }


    function isPlayed() {
        if (kifu[target_i][target_j].color == 'N')
            return false;
        else {
            console.log("You can't play here!");
            return true;
        }
    }


    function isSuicide(object) {
        if (object)
            return false;

        var count = 0;
        if (isOnBoard(target_i-1, target_j) && kifu[target_i-1][target_j].color == swap(tree.getLastNode().color))
            count++;
        else if (!isOnBoard(target_i-1, target_j))
            count++;
        if (isOnBoard(target_i+1, target_j) && kifu[target_i+1][target_j].color == swap(tree.getLastNode().color))
            count++;
        else if (!isOnBoard(target_i+1, target_j))
            count++;
        if (isOnBoard(target_i, target_j-1) && kifu[target_i][target_j-1].color == swap(tree.getLastNode().color))
            count++;
        else if (!isOnBoard(target_i, target_j-1))
            count++;
        if (isOnBoard(target_i, target_j+1) && kifu[target_i][target_j+1].color == swap(tree.getLastNode().color))
            count++;
        else if (!isOnBoard(target_i, target_j+1))
            count++;

        if (count == 4)
            return true;
    }


    var surrender = document.getElementById("surrender");
    surrender.addEventListener('click', function() {
        $.ajax({
            url: '/games/' + game_id + '/surrender',
            method: 'POST',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"))
            },
            data: {
                'surrender': player
            },
            success: function(data) {
                document.getElementById("game-actions").style.visibility = 'hidden';
                if (player == 'W') {
                    document.getElementById("basic_time_black").innerHTML = 'Winner';
                    document.getElementById("basic_time_white").style.visibility = 'hidden';
                    document.getElementById("basic_time_white").style.height = 0;
                    document.getElementById("white_b").style.visibility = 'hidden';
                    document.getElementById("black_b").style.visibility = 'hidden';
                } else {
                    document.getElementById("basic_time_white").innerHTML = 'Winner';
                    document.getElementById("basic_time_black").style.visibility = 'hidden';
                    document.getElementById("basic_time_black").style.height = 0;
                    document.getElementById("white_b").style.visibility = 'hidden';
                    document.getElementById("black_b").style.visibility = 'hidden';
                }
            },
            error: function(data, textStatus){
                alert(textStatus.toUpperCase() + ": " + data);
            }
        });

    });

