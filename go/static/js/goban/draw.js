/**
 * Created by Ilnur on 12.12.2015.
 */

function drawBoard(stars, ctx, background, grid_space, desk, game_size, dots_radius) {
    ctx.drawImage(background, 0, 0);
    ctx.strokeRect(grid_space, grid_space, desk.width-grid_space*2, desk.width-grid_space*2);

    var k = Math.round((desk.width-grid_space)/game_size);
    ctx.beginPath();
    for (var x = grid_space; x < desk.width-grid_space*2; x += k) {
        ctx.moveTo(x, grid_space);
        ctx.lineTo(x, desk.width-grid_space);
    }

    for (var y = grid_space; y < desk.width-grid_space*2; y += k) {
        ctx.moveTo(grid_space, y);
        ctx.lineTo(desk.width-grid_space, y);
    }
    ctx.closePath();
    ctx.stroke();

    ctx.fillStyle = 'black';
    for (var i in stars) {
        ctx.beginPath();
        ctx.ellipse(stars[i][0], stars[i][1], dots_radius, dots_radius, Math.PI*dots_radius*dots_radius, 0, 2*Math.PI);
        ctx.closePath();
        ctx.fill();
    }
}


function drawExistStones (ctx, desk, kifu, stars, game_size, measurements, stone_size, bck, grid_space, dots_radius) {
    desk.width = desk.width;        // clear all canvas
    drawBoard(stars, ctx, bck, grid_space, desk, game_size, dots_radius);

    function stones(color) {
        ctx.fillStyle = defineColor(color);
        ctx.fillStyle = defineColor(color);
        for (var i = 0; i < game_size; i++)
            for (var j = 0; j < game_size; j++)
                if (kifu[i][j].color == color) {
                    ctx.beginPath();
                    ctx.ellipse(measurements[i], measurements[j], stone_size, stone_size, Math.PI*stone_size*stone_size, 0, 2*Math.PI);
                    ctx.closePath();
                    ctx.fill();
                    ctx.stroke();
                }
    }

    stones('W');
    stones('B');
}


function drawPlayedStone(captured_group) {
    if (tree.getLastNode().color == 'W')
        ctx.fillStyle = 'white';
    else
        ctx.fillStyle = 'black';

    ctx.beginPath();
    ctx.ellipse(current_x, current_y, STONE_SIZE, STONE_SIZE, Math.PI*STONE_SIZE*STONE_SIZE, 0, 2*Math.PI);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();

    drawCurrentStoneMark(captured_group);
}


function drawCurrentStoneMark(captured_group) {
    ctx.fillStyle = 'red';
    ctx.fillRect(current_x-4.5, current_y-4.5, 9, 9);

    // redrawing previous stone
    if (tree.getMoves() > 1 && !captured_group) {
        ctx.fillStyle = swap(defineColor(tree.getLastNode().color));
        ctx.fillRect(MEASUREMENTS[copy_last_target_i]-6, MEASUREMENTS[copy_last_target_j]-6, 12, 12);
    }
}

