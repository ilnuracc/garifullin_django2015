
table = document.getElementById('table');
form = document.getElementById('accept-form');


function get_call(callId, caller) {
    if (is_own(caller)) {
        document.getElementById("accept-button").setAttribute("disabled", "true");
        document.getElementById("accept-button").setAttribute("value", "Nope");
    }

    tdLength = table.rows[0].cells.length;
    for (var i = 1; i < table.rows.length; i++) {
        if (table.rows[i].cells[tdLength-1].firstElementChild.id == callId) {
            form.childNodes[3].innerHTML = table.rows[i].cells[8].textContent;
            form.childNodes[7].firstElementChild.innerHTML = table.rows[i].cells[0].textContent;
            form.childNodes[9].firstElementChild.innerHTML = table.rows[i].cells[1].textContent;
            form.childNodes[11].firstElementChild.innerHTML = table.rows[i].cells[2].textContent;
            form.childNodes[13].firstElementChild.innerHTML = table.rows[i].cells[3].textContent;
            form.childNodes[15].firstElementChild.innerHTML = table.rows[i].cells[4].textContent;
            form.childNodes[17].firstElementChild.innerHTML = table.rows[i].cells[5].textContent;
            form.childNodes[19].firstElementChild.innerHTML = table.rows[i].cells[6].textContent;
            form.childNodes[21].firstElementChild.innerHTML = table.rows[i].cells[7].textContent;
        }
    }

    path[2] = callId;
    form.setAttribute("action", path.join("/"));
}

function is_own(caller) {
    return caller == current_user;
}