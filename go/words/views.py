import json
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.http import HttpResponse


# Create your views here
from users.models import Profile
from words.models import Message, Room


@login_required(login_url=reverse_lazy("home:welcome_page"))
def show_page(request, room_id=None):
    response = {}
    in_room = False
    if room_id is None:
        room_id = 1
    profile = Profile.objects.get(user=request.user)
    r_room = Room.objects.get(id=room_id)

    if request.method == 'POST':
        if profile in r_room.users.all():
            word = Message(sender=profile, room=r_room, text=request.POST['text'])
            word.save()

            time = '(%s:%s, %s.%s.%s)' % (word.pub_date.hour, word.pub_date.minute, word.pub_date.day, word.pub_date.month, word.pub_date.year)
            response[0] = {'me': True, 'sender': word.sender.user.username, 'text': word.text, 'pub_date': time}
        return HttpResponse(json.dumps(response))

    elif request.is_ajax() and request.method == 'GET':
        if profile in r_room.users.all():
            i = 0
            messages = Message.objects.filter(room=r_room)
            for item in messages.iterator():
                time = '(%s:%s, %s.%s.%s)' % (
                    item.pub_date.hour, item.pub_date.minute, item.pub_date.day, item.pub_date.month, item.pub_date.year)
                c = {'me': False, 'sender': item.sender.user.username, 'text': item.text, 'pub_date': time}
                if item.sender.user == request.user:
                    c['me'] = True
                response[i] = c
                i += 1
        return HttpResponse(json.dumps(response))

    else:
        messages = None
        rooms = Room.objects.raw("SELECT * FROM words_room")
        users = r_room.users.all()
        if profile in r_room.users.all():
            in_room = True
            messages = Message.objects.filter(room=r_room)
        return render(request, "words/chat.html", {"headline": r_room.title,
                                                   "room_id": room_id,
                                                   "rooms": rooms,
                                                   "in_room": in_room,
                                                   "users": users,
                                                   "words": Message.objects.filter(room=r_room).count(),
                                                   "messages": messages})


@login_required
def get_in(request, room_id=None):
    if request.method == "POST":
        room = Room.objects.get(id=room_id)
        room.users.add(Profile.objects.get(user=request.user))
        return render(request, "words/chat.html", {"headline": room.title,
                                                   "room_id": room_id,
                                                   "rooms": Room.objects.all(),
                                                   "in_room": True,
                                                   "users": room.users.all(),
                                                   "words": Message.objects.filter(room=room).count(),
                                                   "messages": Message.objects.filter(room=room)})
