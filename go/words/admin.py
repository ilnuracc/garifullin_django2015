from django.contrib import admin

# Register your models here.
from words.models import Message, Room, Comment

admin.site.register(Message)
admin.site.register(Room)
admin.site.register(Comment
                    )
