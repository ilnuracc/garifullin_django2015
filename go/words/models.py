from django.db import models

# Create your models here.
from users.models import Profile


class Room(models.Model):
    title = models.CharField(max_length=16)
    users = models.ManyToManyField(Profile)

    def __unicode__(self):
        return self.title


class Message(models.Model):
    sender = models.ForeignKey(Profile)
    room = models.ForeignKey(Room)
    text = models.TextField()
    pub_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s | %s | %s" % (self.room, self.sender, self.text)


class Comment(models.Model):
    sender = models.ForeignKey(Profile)
    text = models.TextField()
    pub_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s at %s: %s" % (self.sender, self.pub_date, self.text)