from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^(?P<room_id>\d+)?$', 'words.views.show_page', name='show_page'),
    url(r'^(?P<room_id>\d+)/become$', 'words.views.get_in', name='get_in'),
]
